(function(Backbone) {


var Todo = Backbone.Model.extend({
	// Default todo attribute values
	defaults: {
		title: '',
		completed: false
	}
});

// Setting the value of attributes via instantiation
var myTodo = new Todo({
	title: "Set through instantiation."
});
console.log('Todo title: ' + myTodo.get('title'));
console.log('Completed: ' + myTodo.get('completed'));

// Set single attribute value at the time through Model.set():
myTodo.set("title", "Title attribute set through Model.set().");
console.log('Todo title: ' + myTodo.get('title'));
console.log('Completed: ' + myTodo.get('completed'));

// Set map of attributes through Model.set():
myTodo.set({
	title: "Both attributes set through Model.set().",
	completed: true
});
console.log('Todo title: ' + myTodo.get('title'));
console.log('Completed: ' + myTodo.get('completed'));


})(Backbone);