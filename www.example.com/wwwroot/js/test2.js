var Donut = Backbone.Model.extend({
	defaults : {
		name : null,
		sprinkles : false,
		cream_filled : false
	}
});

var bostonCream = new Donut({
	name : "Bostan Cream",
	cream_filled : true
});
bostonCream.set({ sprinkles : true });

var Donuts = Backbone.Collection.extend({
	model : Donut
});
var DonutList = new Donuts();

DonutList.add(bostonCream);
DonutList.add({name : 'Plain', sprinkles : false, cream_filled : false});
DonutList.add({name : 'Awesome', sprinkles : true, cream_filled : true});

/*
console.dir(DonutList);

_.each(DonutList.models, iterator);

function iterator (value, key, list) {
	console.log('iterator');
	console.dir(value.attributes);
	console.dir(key);
	console.dir(list);
}
*/

var DonutView = Backbone.View.extend({
	tagName : "div",
	className : "donut",
	render : function() {
		this.el.innerHTML = this.model.get('name');
		return this;
	}
});

var UpdatingDonutView = DonutView.extend({
	initialize : function(options) {
		this.render = _.bind(this.render, this);
		this.model.bind('change:name', this.render);
	}
});

var DonutCollectionView = Backbone.View.extend({
	initialize : function() {
		var that = this;
		this._donutViews = [];
		this.collection.each(function(donut) {
			that._donutViews.push(new UpdatingDonutView({
				model : donut,
				tagName : 'li'
			}));
		});
	},

	render : function() {
		var that = this;
		// Clear out this element.
		$(this.el).empty();

		// Render each sub-view and append it to the parent view's element.
		_(this._donutViews).each(function(dv) {
			$(that.el).append(dv.render().el);
		});
	}
});

var donutCollectionView = new DonutCollectionView({
	collection : DonutList,
	el : $('ul.donuts')[0]
});

donutCollectionView.render();