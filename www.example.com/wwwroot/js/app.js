(function ($) {

	$("#releaseDate").datepicker();

	var Book = Backbone.Model.extend({
		defaults: {
			coverImage: "img/placeholder.png",
			title: "No title",
			author: "Unknown",
			releaseDate: "Unknown",
			keywords: "None"
		},
		idAttribute:"_id"
	});

	var Library = Backbone.Collection.extend({
		model: Book,
		url: '/api/books'
	});

	var BookView = Backbone.View.extend({
		tagName: "div",
		className: "bookContainer",
		template: $("#bookTemplate").html(),
		render: function () {
			var tmpl = _.template(this.template);
			this.$el.html(tmpl(this.model.toJSON()));
			return this;
		},
		events: {
			"click .delete":"deleteBook"
		},
		deleteBook: function () {
			this.model.destroy();
			this.remove();
		}
	});

	var LibraryView = Backbone.View.extend({
		el: $("#books"),
		initialize: function () {
			this.collection = new Library();
			this.collection.fetch();
			this.render();
			this.collection.on("add", this.renderBook, this);
			this.collection.on("remove", this.removeBook, this);
			this.collection.on("reset", this.render, this);
		},
		render: function () {
			var that = this;
			_.each(this.collection.models, function (item) {
				that.renderBook(item);
			}, this);
		},
		events:{
			"click #add":"addBook"
		},
		renderBook: function (item) {
			var bookView = new BookView({
				model: item
			});
			this.$el.append(bookView.render().el);
		},
		addBook: function (e) {
			e.preventDefault();
			var formData = {};
			$("#addBook div").children("input").each(function (i, el) {
				if ($(el).val() !== "") {
					if (el.id === 'keywords') {
						var keywordArray = $(el).val().split(',');
						var keywordObjects = [];
						for (var j = 0; j < keywordArray.length; j++) {
							keywordObjects[j] = {"keyword":keywordArray[j]};
						}
						formData[el.id] = keywordObjects;
					}
					else if (el.id === 'releaseDate'){
						formData[el.id] = $('#releaseDate').datepicker("getDate").getTime();
					}
					else {
						formData[el.id] = $(el).val();
					}
					$(el).val('');
				}
			});
			this.collection.create(formData);
		},
		removeBook: function (removedBook) {
			var removedBookData = removedBook.attributes;
			_.each(removedBookData, function (val, key) {
				if(removedBookData[key] === removedBook.defaults[key]) {
					delete removedBookData[key];
				}
			});
		}
	});

	var libraryView = new LibraryView();


})(jQuery);