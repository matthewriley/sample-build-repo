
public class TestThreads3 {

	public static void main(String[] args) {

		MarketNews3 mn = new MarketNews3("Market News");
		mn.start();
		
		Portfolio3 p = new Portfolio3("Portfolio Data");
		p.start();
		
		System.out.println("The main method of TestThread3 is finished");
		
	}

}
