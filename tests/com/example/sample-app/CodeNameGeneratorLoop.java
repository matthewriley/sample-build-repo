
import java.io.Console;
import java.io.IOException;

public class CodeNameGeneratorLoop {
	public static void main (String[] args) throws IOException {
        Console c = System.console();
        if(c == null){
            System.err.println("No console.");
            System.exit(1);
        }
        String words = c.readLine("Number of words: ");
        int wordsLength = Integer.parseInt(words);
        String[] wordList = {"Alfa", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliet", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "Xray", "Yankee", "Zulu"};
        int wordListLength = wordList.length;
        String wordListPhrase = "";

        for (int i = 1; i <= wordsLength; i++){
            int randWordIndex = (int) (Math.random() * wordListLength);
            wordListPhrase = wordList[randWordIndex] + " " + wordListPhrase;
        }

		System.out.println("The code name is: " + wordListPhrase);
	}
}
